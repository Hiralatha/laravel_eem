<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    protected $table = 'questions';

    protected $fillable = [
        'section','question', 'type','is_active'
    ];

    public function options()
    {
        return $this->hasMany('App\Answer','question_id','id');
    }
}
