<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userqa extends Model
{
    protected $table = 'user_qa';

    protected $fillable = [
        'user_id','question_id', 'answer'
    ];

    public function user()
    {
        return $this->belongsTo("App\User","user_id","id");
    }

    public function question()
    {
        return $this->belongsTo("App\Question","question_id","id");
    }
}
