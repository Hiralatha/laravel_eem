<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

class UserChallengeReset extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'challenge:reset';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Reset challenge after 28 days';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        \Helper::resetUserChallenges();
        /*
        \App\Uchallenges::whereIn('user_id',function($q) use($d) {
            $q->select('id')->from('users')->whereRaw('start_at + interval 27 day < ?', [$d])->get();
        })->update(['is_old'=>1]);

        \App\Wellbeing::whereIn('user_id',function($q) use($d) {
            $q->select('id')->from('users')->whereRaw('start_at + interval 27 day < ?', [$d])->get();
        })->update(['is_old'=>1]);

        \App\User::whereRaw('start_at + interval 27 day < ?', [$d])->update(['start_at'=>$d]);
        */
    }
}
