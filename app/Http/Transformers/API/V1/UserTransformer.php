<?php
namespace  App\Http\Transformers\Api\V1;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\User;
use Storage;
use Config;

class UserTransformer extends \League\Fractal\TransformerAbstract
{

    /**
    * Transform user response
    *
    * @param User $user
    * @return array
    */

    public static function transform(User $user)
    {
        $auser = Auth::user();

        $photo = "";
        if(isset($user->photo) && $user->photo!=""){
            $photo = url('/images/profile_pic/'.$user->photo);
        }

        $data = [
            'id' => $user->id,
            'first_name' => ucwords($user->first_name),
            'last_name' => ucwords($user->last_name),
            'email' => $user->email,
            'social_type' => $user->social_type,
            'social_id' => $user->social_id,
            'is_active' => $user->is_active,
            'is_challenge' => $user->is_challenge,
            'start_at' => $user->start_at,
            'is_reset' => $user->is_reset,
            'photo' => $photo,
            'dob'           =>  $user->dob,
            'gender'        =>  $user->gender,
            'device_type'   =>  @$user->device->device_type,
            'device_token'  =>  @$user->device->device_token,
            'is_reset_popup'  =>  0,
        ];

        if($user->email_verified_at != "")
        {
            $data['is_verify'] = 1;
        }

        $rdate = date('Y-m-d',strtotime($user->start_at));
        $cdate = date('Y-m-d');
        $day = (strtotime($cdate) - strtotime($rdate)) / (60 * 60 * 24);
        $day = $day + 1;
        if($day > 28)
        {
            $day = 28;
            $data['is_reset_popup'] = 1;
        }
        $data['day'] = $day;

        $data['qa'] = [];
        if(@count($user->qa) > 0)
        {
            $i = 0;
            foreach($user->qa as $qa)
            {
                $data['qa'][$i]['question_id'] = $qa->question_id;
                $data['qa'][$i]['question'] = $qa->question->question;
                $data['qa'][$i]['question_type'] = $qa->question->type;
                $data['qa'][$i]['answer'] = $qa->answer;
                $i++;
            }
        }

        $data['wellbeing'] = [];
        $wellbeingArr = [];
        for($i=1; $i <= $day; $i++)
        {
            $wellbeingArr[$i] = array(
                "user_id"=>$user->id,
                "day"=>"$i",
                "is_complete"=>0,
                "mood"=>"0",
                "physical"=>"0",
                "mental"=>"0",
                "stress"=>"0"
            );
        }
        if(@count($user->wellbeing) > 0)
        {
            foreach($user->wellbeing as $wellbeing)
            {
                if($wellbeing->day == @$wellbeingArr[$wellbeing->day]['day'] && $wellbeing->is_old == 0)
                {
                    $wellbeingArr[$wellbeing->day] = array(
                        "user_id"=>$wellbeing->user_id,
                        "day"=>$wellbeing->day,
                        "is_complete"=>$wellbeing->is_complete,
                        "mood"=>$wellbeing->mood,
                        "physical"=>$wellbeing->physical,
                        "mental"=>$wellbeing->mental,
                        "stress"=>$wellbeing->stress
                    );
                }
            }
        }
        foreach ($wellbeingArr as $key => $value) {
            $data['wellbeing'][] = $value;
        }


        $challengesArr = [];
        for($i=1; $i <= $day; $i++)
        {
            $challengesArr[$i] = array(
                "day"=>"$i",
                'challenge_ids' => []
            );
        }


        if(@count($user->challenges,COUNT_RECURSIVE) > 0)
        {
            $c = 0;
            foreach($user->challenges as $uchallenge)
            {
                if($uchallenge->day == @$challengesArr[$uchallenge->day]['day'] && $uchallenge->is_old == 0)
                {
                    $challengesArr[$uchallenge->day]['challenge_ids'][] = $uchallenge->challenge_id;
                    $c++;
                }
            }
        }

        foreach ($challengesArr as $key => $value) {
            $data['challenges'][] = $value;
        }

        if(isset($user->token) && $user->token != "")
        array_set($data, 'token', $user->token);

        return $data;
    }

}

?>
