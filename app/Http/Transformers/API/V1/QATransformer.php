<?php
namespace  App\Http\Transformers\Api\V1;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Question;
use Storage;
use Config;

class QATransformer extends \League\Fractal\TransformerAbstract
{

    /**
    * Transform user response
    *
    * @param User $question
    * @return array
    */

    public static function transform(Question $question)
    {
        $data = [
            'id' => $question->id,
            'question' => $question->question,
            'type' => $question->type,
            'is_active' => $question->is_active,
            ];

        $i = 0;
        $optionArr = [];
        if(@count($question->options) > 0)
        {
            foreach($question->options as $option)
            {
                $optionArr[$i]['id'] = $option->id;
                $optionArr[$i]['label'] = $option->label;
                $i++;
            }
            $data['options'] = $optionArr;
        }

        return $data;
    }

}

?>
