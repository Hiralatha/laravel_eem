<?php
namespace  App\Http\Transformers\Api\V1;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Question;
use App\Challenges;
use Storage;
use Config;

class ChallengeTransformer extends \League\Fractal\TransformerAbstract
{

    /**
    * Transform Challenges response
    *
    * @param Challenges $challenge
    * @return array
    */

    public static function transform(Challenges $challenge)
    {
        $user = Auth::user();
        $rdate = date('Y-m-d',strtotime($user->start_at));
        $cdate = date('Y-m-d');
        $day = (strtotime($cdate) - strtotime($rdate)) / (60 * 60 * 24);
        $day = $day + 1;
        if($day > 28)
        {
            $day = 28;
        }

        $data = [
            'id' => $challenge->id,
            'title' => $challenge->title,
            'description' => $challenge->description,
            'day' => $day
        ];

        return $data;
    }

}

?>
