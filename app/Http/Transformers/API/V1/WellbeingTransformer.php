<?php
namespace  App\Http\Transformers\Api\V1;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Question;
use App\Challenges;
use App\Wellbeing;
use Storage;
use Config;

class WellbeingTransformer extends \League\Fractal\TransformerAbstract
{

  /**
  * Transform Wellbeing response
  *
  * @param Wellbeing $wellbeing
  * @return array
  */

  public static function transform(Wellbeing $wellbeing)
  {
    $data = [
      'id' => $wellbeing->id,
      'day' => $wellbeing->day,
      'is_complete' => $wellbeing->is_complete,
      'mood' => $wellbeing->mood,
      'physical' => $wellbeing->physical,
      'mental' => $wellbeing->mental,
      'stress' => $wellbeing->stress,
    ];

    return $data;
  }

}

?>
