<?php
namespace  App\Http\Transformers\Api\V1;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use App\Question;
use App\Challenges;
use App\Uchallenges;
use Storage;
use Config;

class UserChallengeTransformer extends \League\Fractal\TransformerAbstract
{

  /**
  * Transform Uchallenges response
  *
  * @param Uchallenges $uchallenges
  * @return array
  */

  public static function transform(Uchallenges $uchallenge)
  {
    $data = [
      'id' => $uchallenge->id,
      'challenge_id' => $uchallenge->challenge_id,
      'day' => $uchallenge->day,
    ];

    return $data;
  }

}

?>
