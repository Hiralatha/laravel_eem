<?php

namespace App\Http\Controllers\Api\V1;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Http\Response;
use Storage;
use Config;

class ResponseController extends Controller
{
  public static function apiresponse($success = true, $message = "Request Completed Successfully!", $content = null, $status = 200, $type = null, $notificationcount = 0)
  {
    if($message == "Token has expired"){
      $message = "Your session has expired. Please re-login to continue.";
    }

    if(is_array($message)){
      $message = $message[0];
    }

    if($success == true){
      $interResponse = array(
        'success' => $success,
        'message' => $message,
        'data' => json_decode($content)
      );
    }else{
      $interResponse = array(
        'success' => $success,
        'message' => $message
      );
    }

    if($type && $type == 'match'){
      $interResponse['notificationcount'] = $notificationcount;
    }

    $response = new Response($interResponse);
    $response->setStatusCode($status);

    return $response;
  }
}
