<?php

namespace App\Http\Controllers\API\V1;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use League\Fractal\Manager;
use League\Fractal\Resource\Collection;
use Spatie\Fractal\Fractal;
use Validator, Config, Image, Mail, DB;
use App\Http\Transformers\API\V1\UserTransformer;
use App\Http\Transformers\API\V1\QATransformer;
use App\Http\Transformers\API\V1\ChallengeTransformer;
use App\Http\Transformers\API\V1\UserChallengeTransformer;
use App\Http\Transformers\API\V1\WellbeingTransformer;
use App\User;
use App\Userdevices;
use App\Userqa;
use App\Challenges;
use App\Uchallenges;
use App\Wellbeing;

class UserController extends Controller
{
    public function signup(Request $request)
    {
        $data = $request->only(['first_name','last_name','dob','email','password','device_type','device_id','device_token','social_type','social_id']);

        $validator = Validator::make($data, [
            'first_name' => 'required',
            'last_name' => 'required',
            // 'dob' => 'required',
            'email' => 'required|unique:users',
            'password' => 'required|min:8',
            'device_type' => 'in:IOS,ANDROID',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        User::unguard();
        $udata['first_name'] = ucwords($request->first_name);
        $udata['last_name'] = ucwords($request->last_name);
        $udata['email'] = strtolower($request->email);
        if($request->dob != "")
            $udata['dob'] = $request->dob;
        $udata['password'] = \Hash::make($request->password);
        $udata['social_type'] = $request->social_type;
        $udata['social_id'] = $request->social_id;
        $udata['is_active'] = 1;
        $udata['start_at'] = date('Y-m-d');
        $user = User::create($udata);
        //$user->sendEmailVerificationNotification();
        User::reguard();

        if(isset($request->device_token) && $request->device_token != ''){
            $devicedata = array("device_id" =>$request->device_id,"device_type" => $request->device_type,'device_token' => $request->device_token,"user_id" => $user->id);
            $userDevices = Userdevices::where("user_id",$user->id)->where("device_id",$request->device_id)->get()->toArray();
            if(count($userDevices) > 0){
                Userdevices::where('id', $userDevices[0]['id'])->update($devicedata);
            }else{
                $instance = Userdevices::create($devicedata);
                $instance->save();
            }
        }

        $credentials = array('email' => $udata['email'],'password' => $request->password,'user_type' => 'User','social_type'=>'Normal');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return ResponseController::apiresponse(false, 'Invalid Access!');
            }
        } catch (JWTException $e) {
            return ResponseController::apiresponse(false, 'Could not create token!');
        }
        $user->token = $token;
        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function socialSignupLogin(Request $request)
    {
        $data = $request->only(['first_name','last_name','email','dob','social_type','social_id','device_type','device_id','device_token']);

        $validator = Validator::make($data, [
            // 'email' => 'required',
            'device_type' => 'in:IOS,ANDROID',
            'social_type' =>  'required|in:Google,Facebook,Apple',
            'social_id' =>  'required',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        if(strtolower($request->first_name) == "undefined") $data['first_name'] = "";
        if(strtolower($request->last_name) == "undefined") $data['last_name'] = "";
        if(strtolower($request->email) == "undefined" || $request->email == "" || $request->email == "null" || $request->email == NULL)
        {
            $data['email'] = "";
        }

        $user = User::where('social_type',$data['social_type'])->where('social_id',$data['social_id'])->first();
        if(!$user && $data['email'] != "")
        {
            $user = User::where('email',$data['email'])->first();
        }

        if($user)
        {
            try {
                if (! $token = JWTAuth::fromUser($user)) {
                    return ResponseController::apiresponse(false, 'Invalid Access!');
                }
            } catch (JWTException $e) {
                return ResponseController::apiresponse(false, 'Could not create token!');
            }
            if(!(User::where('email',$data['email'])->exists()))
            {
                $user->email = $data['email'];
            }
            $user->social_type = $data['social_type'];
            $user->social_id = $data['social_id'];
            $user->save();

            if(isset($request->device_token) && $request->device_token != '')
            {
                $devicedata = array("device_type" => $request->device_type,'device_token' => $request->device_token,"user_id" => $user->id);
                UserDevices::updateOrCreate(['user_id' => $user->id],$devicedata);
            }

            $user->token = $token;
            $data = fractal($user, new UserTransformer())->toArray();
            return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
        }
        else
        {
            User::unguard();
            $udata = [];
            $udata['first_name'] = ucwords($data['first_name']);
            $udata['last_name'] = ucwords($data['last_name']);
            $udata['email'] = strtolower($data['email']);
            if($data['dob'] != "") $udata['dob'] = $data['dob'];
            $udata['password'] = \Hash::make(str_pad(mt_rand(1,99999999),8,'0',STR_PAD_LEFT));
            $udata['social_type'] = $data['social_type'];
            $udata['social_id'] = $data['social_id'];
            $udata['is_active'] = 1;
            $udata['start_at'] = date('Y-m-d');
            $user = User::create($udata);
            User::reguard();

            if(isset($request->device_token) && $request->device_token != ''){
                $devicedata = array("device_id" =>$request->device_id,"device_type" => $request->device_type,'device_token' => $request->device_token,"user_id" => $user->id);
                $userDevices = Userdevices::where("user_id",$user->id)->where("device_id",$request->device_id)->get()->toArray();
                if(count($userDevices) > 0){
                    Userdevices::where('id', $userDevices[0]['id'])->update($devicedata);
                }else{
                    $instance = Userdevices::create($devicedata);
                    $instance->save();
                }
            }
            try {
                if (! $token = JWTAuth::fromUser($user)) {
                    return ResponseController::apiresponse(false, 'Invalid Access!');
                }
            } catch (JWTException $e) {
                return ResponseController::apiresponse(false, 'Could not create token!');
            }
            $user->token = $token;
            $data = fractal($user, new UserTransformer())->toArray();
            return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
        }
    }

    public function login(Request $request)
    {
        $credential = $request->only(['email','password','device_type','device_id','device_token']);

        $validator = Validator::make($credential, [
        'email' => 'required',
        'password' => 'required|min:8',
        'device_type' => 'in:IOS,ANDROID',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        $credentials = array('email' => $request->email,'password' => $request->password,'user_type' => 'User');
        try {
            if (! $token = JWTAuth::attempt($credentials)) {
                return ResponseController::apiresponse(false, 'Invalid Credentials!');
            }
        } catch (JWTException $e) {
            return ResponseController::apiresponse(false, 'Could not create token!');
        }

        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        if(isset($request->device_token) && $request->device_token != ''){
            $devicedata = array("device_id" =>$request->device_id,"device_type" => $request->device_type,'device_token' => $request->device_token,"user_id" => $user->id);
            $userDevices = Userdevices::where("user_id",$user->id)->where("device_id",$request->device_id)->get()->toArray();
            if(count($userDevices) > 0){
                Userdevices::where('id', $userDevices[0]['id'])->update($devicedata);
            }else{
                $instance = Userdevices::create($devicedata);
                $instance->save();
            }
        }

        if($user->is_active == 0){
            JWTAuth::invalidate();
            return ResponseController::apiresponse(false, 'Your account is inactive. Please contact administrator to get access!');
        }
        $user->token = $token;
        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function logout(Request $request){
        JWTAuth::invalidate();
        return ResponseController::apiresponse(true, "Logged out Successfully!", json_encode([]));
    }



    /* Forgot Password */
    public function forgot(Request $request)
    {
        $credential = $request->only(['email']);
        $validator = Validator::make($credential, [
        'email' => 'required',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        $user = User::where('email',$credential['email'])->first();

        if(@$user->id != ""){
            $pass = \Helper::generateRandomString();
            $udata['password'] = \Hash::make($pass);
            $users = User::where('id',$user->id)->update($udata);

            $from_email = Config::get('constant.FROM_ADDRESS');
            $from_name  = Config::get('constant.FROM_NAME');
            $subject = config('app.name').' - Forgot Password';
            $emailData = array('to' => $user->email,'from' => $from_email,'subject' => $subject,'from_name' => $from_name);
            try{
                Mail::send('emails.password', ["email"=>$user[0]['email'],'password' => $pass,], function ($message) use ($emailData) {
                    $message
                    ->to($emailData['to'])
                    ->from($emailData['from'], $emailData['from_name'])
                    ->subject($emailData['subject']);
                });
            }catch (\Exception $e) {

            }

            return ResponseController::apiresponse(true, 'The new password has been sent to your registered email address!',json_encode([]));
        }else{
            return ResponseController::apiresponse(false, 'Invalid Access!');
        }
    }

    public function getUserDetails()
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    /* Update Profile */
    public function updateProfile(Request $request)
    {
        $user = Auth::user();
        $data = $request->only(['first_name','last_name','email','dob','gender','pic','device_type','device_id','device_token']);
        $validator = Validator::make($data, [
        'first_name' => 'required',
        'last_name' => 'required',
        // 'dob' => 'required',
        'gender' => 'required',
        'email' => 'required|unique:users,email,'.$user->id,
        'device_type' => 'in:IOS,ANDROID',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        if ($request->hasFile('pic'))
        {
            $destinationPath = public_path('/images/profile_pic');
            if(!is_dir($destinationPath))
            {
                mkdir($destinationPath, 0777, true);
            }
            $image = $request->file('pic');
            $name = time().'.'.$image->getClientOriginalExtension();
            $image->move($destinationPath, $name);
            $pic = $name;
        }
        else
        {
            $pic = "";
            if(isset($user->photo) && $user->photo!=""){
                $pic = $user->photo;
            }
        }

        User::unguard();
        $udata['first_name'] = ucwords($request->first_name);
        $udata['last_name'] = ucwords($request->last_name);
        $udata['email'] = strtolower($request->email);
        if($request->dob != "")
            $udata['dob'] = $request->dob;
        $udata['gender'] = $request->gender;
        $udata['photo'] = $pic;
        $users = User::where('id',$user->id)->update($udata);
        User::reguard();

        if(isset($request->device_token) && $request->device_token != ''){
            $devicedata = array("device_id" =>$request->device_id,"device_type" => $request->device_type,'device_token' => $request->device_token,"user_id" => $user->id);
            $userDevices = Userdevices::where("user_id",$user->id)->where("device_id",$request->device_id)->get()->toArray();
            if(count($userDevices) > 0){
                Userdevices::where('id', $userDevices[0]['id'])->update($devicedata);
            }else{
                $instance = Userdevices::create($devicedata);
                $instance->save();
            }
        }
        $user = User::find($user->id);
        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Profile has been updated successfully!',json_encode($data['data']));
    }

    /* Update Password */
    public function changePassword(Request $request)
    {
        $user = Auth::user();
        $credential = $request->only(['old_password','password']);
        $validator = Validator::make($credential, [
        'password' => 'required|min:8',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        if(!(\Hash::check($request->old_password, $user->password)))
        {
            return ResponseController::apiresponse(false, 'The specified old password does not match with current password!',json_encode([]));
        }


        User::unguard();
        $udata['password'] = \Hash::make($request->password);
        $users = User::where('id',$user->id)->update($udata);
        User::reguard();

        $user->token = $request->bearerToken();

        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Password has been updated successfully!',json_encode($data['data']));
    }

    public function questionAnswers(Request $request)
    {
        $data = $request->only(['section']);
        $validator = Validator::make($data, [
        'section' => 'required',
        ]);
        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }
        $questions = \App\Question::where('is_active',1)->where('section',$data['section'])->with(['options'])->get();
        $data = fractal($questions, new QATransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function wellbeing(Request $request)
    {
        $user = Auth::user();
        $data = $request->only(['mood','physical','mental','stress']);
        $validator = Validator::make($data, [
        'mood' => 'required|numeric',
        'physical' => 'required|numeric',
        'mental' => 'required|numeric',
        'stress' => 'required|numeric',

        ]);
        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        $rdate = date('Y-m-d',strtotime($user->start_at));
        $cdate = date('Y-m-d');
        $day = (strtotime($cdate) - strtotime($rdate)) / (60 * 60 * 24);
        $day = $day + 1;


        $is_exist = Wellbeing::where([['user_id',$user->id],['day',$day]])->where('is_old',0)->get()->toArray();
        if(isset($is_exist[0]['id']) && $is_exist[0]['id']!=""){
            $data['is_complete'] = 1;
            Wellbeing::where([['user_id',$user->id],['day',$day],['id',$is_exist[0]['id']]])->where('is_old',0)->update($data);
        }else{
            $data['user_id'] = $user->id;
            $data['day'] = $day;
            $data['is_complete'] = 1;
            Wellbeing::create($data);
        }
        return ResponseController::apiresponse(true, 'Success!',json_encode([]));
    }

    public function dailyChallenges(Request $request)
    {
        $user = Auth::user();

        $validator = Validator::make($request->all(), [
            'challengeArr' => 'required|array',
            'challengeArr.*.challenge_id' => 'required|numeric',
            'challengeArr.*.is_delete' => 'required|in:0,1',
        ]);

        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }

        $rdate = date('Y-m-d',strtotime($user->start_at));
        $cdate = date('Y-m-d');
        $day = (strtotime($cdate) - strtotime($rdate)) / (60 * 60 * 24);
        $day = $day + 1;
        if($day > 28)
        {
            $day = 28;
        }

        if(count($request->challengeArr) > 0)
        {
            foreach($request->challengeArr as $challenge)
            {
                if(isset($challenge['is_delete']) && $challenge['is_delete'] == 1)
                {
                    Uchallenges::where([['user_id',$user->id],['challenge_id',$challenge['challenge_id']],['day',$day]])->delete();
                }
                else
                {
                    $cdata = [];
                    $cdata['challenge_id'] = $challenge['challenge_id'];
                    $cdata['user_id'] = $user->id;
                    $cdata['day'] = $day;
                    Uchallenges::where($cdata)->delete();
                    Uchallenges::create($cdata);
                }
            }
        }

        $uchallenges = Uchallenges::where('user_id',$user->id)->where('is_old',0)->orderBy('day','asc')->orderBy('challenge_id', 'asc')->get();
        $data = fractal($uchallenges, new UserChallengeTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function userWellbeing(Request $request)
    {
        $user = Auth::user();
        $wellbeing = Wellbeing::where('user_id',$user->id)->where('is_old',0)->orderBy('day','asc')->get();
        $data = fractal($wellbeing, new WellbeingTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function userChallenge(Request $request)
    {
        $user = Auth::user();
        $uchallenges = Uchallenges::where('user_id',$user->id)->where('is_old',0)->orderBy('day','asc')->orderBy('challenge_id', 'asc')->get();
        $data = fractal($uchallenges, new UserChallengeTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function storeUserQA(Request $request)
    {
        $user = Auth::user();
        $data = $request->only(['que_ans_arr']);
        $validator = Validator::make($data, [
        'que_ans_arr' => 'required|array',
        ]);
        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }
        Userqa::where('user_id',$user->id)->delete();
        if(@count($data['que_ans_arr']) > 0)
        {
            foreach($data['que_ans_arr'] as $que_ans_arr)
            {
                if(@$que_ans_arr['question_id'] != "" && @$que_ans_arr['answer'] != "")
                {
                    $insertData = [];
                    $insertData['user_id'] = $user->id;
                    $insertData['question_id'] = $que_ans_arr['question_id'];
                    $insertData['answer'] = $que_ans_arr['answer'];
                    Userqa::create($insertData);
                }
            }
        }
        return ResponseController::apiresponse(true, 'Your answer has been saved successfully!',json_encode([]));
    }

    public function isChallenge(Request $request)
    {
        $user = Auth::user();
        $data = $request->only(['is_challenge']);
        $validator = Validator::make($data, [
        'is_challenge' => 'required|in:1,0',
        ]);
        if($validator->fails()) {
            $message = $validator->errors()->all()[0];
            return ResponseController::apiresponse(false, $message);
        }
        User::where('id',$user->id)->update($data);
        $user = User::where('id',$user->id)->get();
        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function deleteAccount()
    {
        $user_id = Auth::user()->id;
        $user = User::find($user_id);
        $user->delete();
        return ResponseController::apiresponse(true, 'Your account has been deleted successfully!',json_encode([]));
    }

    public function reset()
    {
        $user_id = Auth::user()->id;
        $data['is_reset'] = 1;
        $user = User::where('id',$user_id)->update($data);
        // Uchallenges::where('user_id',$user_id)->delete();
        // Wellbeing::where('user_id',$user_id)->delete();
        \Helper::resetUserChallenges($user_id);
        $user = User::where('id',$user_id)->get();
        $data = fractal($user, new UserTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function challenges(){
        $challenges = Challenges::orderBy('id', 'ASC')->get();

        $data = fractal($challenges, new ChallengeTransformer())->toArray();
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function dailyEnergy(){

        $data['data'][0]['title'] = "Full Daily Energy Routine Video";
        $data['data'][0]['link'] = "Di5Ua44iuXc"; // Youtube Id
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function energyMinutes(){

        $data['data'][0]['title'] = "Energy Minutes";
        $data['data'][0]['link'] = "bAs0e8LLdVI"; // Youtube Id
        $data['data'][1]['title'] = "Energy Medicine Resource";
        $data['data'][1]['link'] = "saqL8AEEGTs"; // Youtube Id
        return ResponseController::apiresponse(true, 'Success!',json_encode($data['data']));
    }

    public function aboutus(Request $request)
    {
        $html = '<p style="font-weight:300"><strong>Eden Method - Supporting Health and Happiness Since 1977</strong></p>';

        $html .= '<p style="font-weight:300"><strong>Donna Eden is a world-renowned healer and educator. After using her own energy to heal herself of severe Multiple Sclerosis, she became passionate and dedicated to share what she had learned with others. She was certain that the concepts and techniques that had led to her own recovery could help others with theirs.</strong></p>';

        $html .= '<p style="font-weight:300">Since 1977, <strong>her work has touched millions of people across the globe</strong>, introducing them to the healing and restorative power of their body&rsquo;s own energies.</p>';

        $html .= '<p style="font-weight:300">With some basic understanding and simple methods, you can mobilize your own natural energies to serve you with their full power. We at Eden Method are committed to offering the best tools available for engaging your life force to empower yourself to <em>feel</em> at your best and to <em>live</em> at your best.</p>';

        $html .= '<p style="font-weight:300">One of these tools is Donna Eden&rsquo;s Daily Energy Routine. When practiced regularly, this simple set of exercises can create healthy energy patterns that will bring you more energy, joy, and vitality.</p>';

        $html .= '<p style="font-weight:300"><a href="https://edenmethod.com/free-energy-class/">https://edenmethod.com/free-energy-class/</a></p>';

        $html .= '<p style="font-weight:300"><a href="https://edenmethod.com">https://edenmethod.com</a></p>';

        $html .= '<p style="font-weight:300"><strong>App Powered by <a href="https://sourcewisdom.co.uk/">SourceWisdom</a></strong></p>';

        $html .= '<p style="font-weight:300"><a href="https://sourcewisdom.co.uk/">SourceWisdom</a> founder Britta Campe initiated the development of the first ever EDEN METHOD APP in an effort to help people remember and practice Donna Eden&rsquo;s Daily Energy Routine.</p>';

        $html .= '<p style="font-weight:300">Together with the Eden Method team, they have created a mobile APP designed to not only show you the exercises but also track and record your progress.&nbsp;</p>';

        $html .= '<p style="font-weight:300">Our goal with the EDEN METHOD APP is to give you a tool that will help you establish a daily habit of practicing this simple and powerful routine.</p>';
        return ResponseController::apiresponse(true, '',json_encode($html));
    }
}
