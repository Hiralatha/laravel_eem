<?php

namespace App\Http\Middleware;

use Closure;

class LogRequests
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $request->start = microtime(true);
        return $next($request);
    }

    public function terminate($request, $response)
    {
        $request->end = microtime(true);
        $this->log($request,$response);
    }

    protected function log($request,$response)
    {
        $duration = $request->end - $request->start;
        $user = auth('api')->user();
        
        $reqData = [];
        $reqData['url'] = $request->fullUrl();
        $reqData['method'] = $request->getMethod();
        $reqData['user_agent'] = $request->header('User-Agent');
        $reqData['request'] = json_encode($request->all());
        $reqData['response'] = $response->getContent();
        $reqData['start_time'] = date('Y-m-d H:i:s',$request->start);
        $reqData['end_time'] = date('Y-m-d H:i:s',$request->start);
        $reqData['duration'] = $duration;
        $reqData['ip'] = $request->getClientIp();

        $reqData['user_id'] = "";
        if(isset($user->id) && $user->id > 0)
            $reqData['user_id'] = @$user->id;

        \App\Requestlog::create($reqData);
    }
}
