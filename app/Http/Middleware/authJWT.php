<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Tymon\JWTAuth\Facades\JWTAuth;
use Exception;
use Illuminate\Http\Request;

class authJWT
{
    /**
    * Handle an incoming request.
    *
    * @param  \Illuminate\Http\Request  $request
    * @param  \Closure  $next
    * @return mixed
    */
    public function handle($request, Closure $next)
    {
        $interResponse = array(
            'success' => false
        );
        try {
            if (!$request->bearerToken()) {
                $interResponse['message'] = 'Token is not provided!';
                return response()->json($interResponse);
            }
            $token = $request->bearerToken();
            $user = JWTAuth::parseToken()->authenticate();

            if (! $user) {
                $interResponse['message'] = 'User not found!';
                return response()->json($interResponse);
            }

        } catch (\Exception $e) {

            if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenInvalidException){
                $interResponse['message'] = 'Token is invalid!';
                return response()->json($interResponse);
            }else if ($e instanceof \Tymon\JWTAuth\Exceptions\TokenExpiredException){
                $interResponse['message'] = 'Token is expired!';
                return response()->json($interResponse);
            }else{
                $interResponse['message'] = 'Something is wrong!';
                return response()->json($interResponse);
            }
        }
        return $next($request);
    }
}
