<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Wellbeing extends Model
{
    protected $table = 'user_wellbeing';

    protected $fillable = [
        'user_id','day','mood','physical','mental','stress','is_complete'
    ];

}
