<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;

use Tymon\JWTAuth\Contracts\JWTSubject;

class User extends Authenticatable implements JWTSubject
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'user_type', 'first_name', 'last_name', 'email', 'dob', 'photo', 'gender', 'social_type', 'social_id','is_active', 'password','is_challenge','start_at','is_reset'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    public function getJWTIdentifier()
    {
        return $this->getKey();
    }

    public function getJWTCustomClaims()
    {
        return [];
    }

    public function device()
    {
        return $this->hasOne("App\Userdevices","user_id","id");
    }

    public function qa()
    {
        return $this->hasMany("App\Userqa","user_id","id");
    }

    public function wellbeing()
    {
        return $this->hasMany("App\Wellbeing","user_id","id");
    }

    public function challenges()
    {
        return $this->hasMany("App\Uchallenges","user_id","id");
    }
}
