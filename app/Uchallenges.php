<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Uchallenges extends Model
{
    protected $table = 'user_challenges';

    protected $fillable = [
        'user_id','challenge_id','day'
    ];

}
