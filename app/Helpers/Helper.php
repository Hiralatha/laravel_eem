<?php
namespace App\Helpers;
use Illuminate\Support\Facades\Auth;
use Storage;
use Config;
use App\User;
use DB;
use Session;
use Mail;

class Helper
{
    /* Clean File name */
    public static function sanitizeFilename($f)
    {
      // a combination of various methods
      // we don't want to convert html entities, or do any url encoding
      // we want to retain the "essence" of the original file name, if possible
      // char replace table found at:
      // http://www.php.net/manual/en/function.strtr.php#98669
      $replace_chars = array(
        'Š' => 'S', 'š' => 's', '�?' => 'Dj', 'Ž' => 'Z', 'ž' => 'z', 'À' => 'A', '�?' => 'A', 'Â' => 'A', 'Ã' => 'A', 'Ä' => 'A',
        'Å' => 'A', 'Æ' => 'A', 'Ç' => 'C', 'È' => 'E', 'É' => 'E', 'Ê' => 'E', 'Ë' => 'E', 'Ì' => 'I', '�?' => 'I', 'Î' => 'I',
        '�?' => 'I', 'Ñ' => 'N', 'Ò' => 'O', 'Ó' => 'O', 'Ô' => 'O', 'Õ' => 'O', 'Ö' => 'O', 'Ø' => 'O', 'Ù' => 'U', 'Ú' => 'U',
        'Û' => 'U', 'Ü' => 'U', '�?' => 'Y', 'Þ' => 'B', 'ß' => 'Ss', 'à' => 'a', 'á' => 'a', 'â' => 'a', 'ã' => 'a', 'ä' => 'a',
        'å' => 'a', 'æ' => 'a', 'ç' => 'c', 'è' => 'e', 'é' => 'e', 'ê' => 'e', 'ë' => 'e', 'ì' => 'i', 'í' => 'i', 'î' => 'i',
        'ï' => 'i', 'ð' => 'o', 'ñ' => 'n', 'ò' => 'o', 'ó' => 'o', 'ô' => 'o', 'õ' => 'o', 'ö' => 'o', 'ø' => 'o', 'ù' => 'u',
        'ú' => 'u', 'û' => 'u', 'ý' => 'y', 'ý' => 'y', 'þ' => 'b', 'ÿ' => 'y', 'ƒ' => 'f'
      );
      $f = strtr($f, $replace_chars);
      // convert & to "and", @ to "at", and # to "number"
      $f = preg_replace(array('/[\&]/', '/[\@]/', '/[\#]/'), array('-and-', '-at-', '-number-'), $f);
      $f = preg_replace('/[^(\x20-\x7F)]*/', '', $f); // removes any special chars we missed
      $f = str_replace(' ', '-', $f); // convert space to hyphen
      $f = str_replace('\'', '', $f); // removes apostrophes
      $f = preg_replace('/[^\w\-\.]+/', '', $f); // remove non-word chars (leaving hyphens and periods)
      $f = preg_replace('/[\-]+/', '-', $f); // converts groups of hyphens into one
      return strtolower($f);
    }
    /* End */

    /**
    *   Name: generateCode
    *   Desc: send varification mail to new user.
    *
    */
    public static function generateCode()
    {
        $str = '';
        $chars =  time().str_random(6);
        for ($i=0; $i < 6; $i++) {
            $str .= $chars[rand(0, strlen($chars) - 1)];
        }
        return $str;
    }

    public static function generateRandomString($length = 10)
    {
        $characters = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    public static function sendPushNotification($token, $notification,$data=[])
    {
        $fcmUrl = config('constant.GOOGLE_FCM_URL');
        $key = config('constant.GOOGLE_FCM_KEY');

        $fcmNotification = [
            'to' => $token,
            'notification' => $notification,
            'data' => $data,
        ];

        $headers = [
            'Authorization: key='.$key,
            'Content-Type: application/json'
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $fcmUrl);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
        curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($fcmNotification));
        $result = curl_exec($ch);
        curl_close($ch);
    }

    public static function imgCompress($source, $destination, $quality = 60)
    {
        $info = getimagesize($source);

		if ($info['mime'] == 'image/jpeg')
        {
            $image = imagecreatefromjpeg($source);
            imagejpeg($image, $destination, $quality);
        }
		elseif ($info['mime'] == 'image/gif')
        {
            $image = imagecreatefromgif($source);
            imagejpeg($image, $destination, $quality);
        }
		elseif ($info['mime'] == 'image/png')
        {
            $image = imagecreatefrompng($source);
            imagejpeg($image, $destination, $quality);
        }

		return $destination;
	}

    public static function resetUserChallenges($user_id = 0)
    {
        $d = date('Y-m-d');
        $users = \App\User::where('is_reset',1);
        if($user_id > 0)
        {
            $users = $users->where('id',$user_id);
        }
        $users = $users->whereRaw('start_at + interval 27 day < ?', [$d])->get();
        if($users->count() > 0)
        {
            foreach($users as $user)
            {
                \App\Uchallenges::where('user_id',$user->id)->update(['is_old'=>1]);
                \App\Wellbeing::where('user_id',$user->id)->update(['is_old'=>1]);
                $user->is_reset = 0;
                $user->start_at = $d;
                $user->save();
            }
        }
    }
}
