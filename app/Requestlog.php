<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Requestlog extends Model
{
    protected $table = 'request_log';

    protected $fillable = [
        'user_id', 'url','method','user_agent','request','response','start_time','end_time','duration','ip'
    ];
}
