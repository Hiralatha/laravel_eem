<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Userdevices extends Model
{
    protected $table = 'userdevices';

    protected $fillable = [
        'user_id', 'device_type', 'device_id','device_token'
    ];
}
