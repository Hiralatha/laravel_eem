<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});


Route::get('/clear', function() {
    $stream = fopen("php://output", "w");
    Artisan::call("config:clear", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    Artisan::call("cache:clear", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    Artisan::call("config:cache", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    Artisan::call("route:clear", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    Artisan::call("view:clear", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    Artisan::call("clear-compiled", array(), new Symfony\Component\Console\Output\StreamOutput($stream));

    return Artisan::output();
});

// Migration :
Route::get('/migrate', function() {
    $stream = fopen("php://output", "w");
    Artisan::call("migrate", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    return Artisan::output();
});

Route::get('/challenge-reset', function() {
    $stream = fopen("php://output", "w");
    Artisan::call("challenge:reset", array(), new Symfony\Component\Console\Output\StreamOutput($stream));
    return Artisan::output();
});
