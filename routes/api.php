<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

/*Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/


Route::group(['namespace' => 'API\V1','prefix' => 'v1'], function() {
    Route::post('signup', 'UserController@signup');
    Route::post('question_answer', 'UserController@questionAnswers');
    Route::post('social_signup_login', 'UserController@socialSignupLogin');
    Route::post('login', 'UserController@login');
    Route::post('forgot','UserController@forgot');

    Route::get('aboutus','UserController@aboutus');
});

Route::group(['namespace' => 'API\V1','prefix' => 'v1','middleware' => 'own.middleware'], function(){

    Route::post('/user_details','UserController@getUserDetails');
    Route::post('/updateme','UserController@updateProfile');
    Route::post('/changepassword','UserController@changePassword');
    Route::post('/user_question_answer','UserController@storeUserQA');
    Route::post('/reminder','UserController@reminder');
    Route::post('/delete_account','UserController@deleteAccount');
    Route::post('/logout','UserController@logout');

    Route::get('/daily_energy','UserController@dailyEnergy');
    Route::get('/energy_minutes','UserController@energyMinutes');

    Route::post('/ischallenge','UserController@isChallenge');
    Route::get('/challenges','UserController@challenges');
    Route::get('/userchallenges','UserController@userChallenge');
    Route::post('/dailychallenges','UserController@dailyChallenges');

    Route::get('/userwellbeing','UserController@userWellbeing');
    Route::post('/wellbeing','UserController@wellbeing');

    Route::post('/reset','UserController@reset');
});
