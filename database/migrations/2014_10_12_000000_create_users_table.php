<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->enum('user_type',['Admin','User'])->default('User');
            $table->string('first_name')->nullable();
            $table->string('last_name')->nullable();
            $table->string('email')->nullable();
            $table->date('dob')->nullable();
            $table->string('photo')->nullable();
            $table->enum('gender',['Male','Female','Other'])->nullable();
            $table->enum('social_type',['Normal','Google','Facebook','Apple'])->default('Normal');
            $table->string('social_id')->nullable();
            $table->boolean('is_active')->default(true);
            $table->boolean('is_challenge')->default(true);
            $table->timestamp('email_verified_at')->nullable();
            $table->date('start_at');
            $table->boolean('is_reset')->default(false);
            $table->string('password');
            $table->string('password')->nullable();
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
