<?php

use Illuminate\Database\Seeder;
use App\Question;
use App\Answer;
use App\Challenges;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        Schema::disableForeignKeyConstraints();
        Question::truncate();
        Answer::truncate();
        Challenges::truncate();
        Schema::enableForeignKeyConstraints();

        $questions = [];

        $questions[0] = ["section"=>"signup", "question"=>"What are your reason(s) and goals for using this App?", "type"=>"option", "is_active"=>1 , "options" => ['Improve physical health', 'Improve mental health', 'Improve both (physical and mental health)', 'Maintain health', 'Curiosity']];

        $questions[1] = ["section"=>"signup", "question"=>"How is your current Mood?", "type"=>"slider", "is_active"=>1, "options" => ['Mood', 'Poor', 'Very Good']];

        $questions[2] = ["section"=>"signup", "question"=>"How would you rate your current wellbeing?", "type"=>"slider", "is_active"=>1, "options" => ['Wellbeing', 'Poor', 'Very Good']];

        $questions[3] = ["section"=>"signup", "question"=>"How would you rate your current health?", "type"=>"slider", "is_active"=>1, "options" => ['Health', 'Poor', 'Very Good']];

        $questions[4] = ["section"=>"signup", "question"=>"Please rate you current daily stress level", "type"=>"slider", "is_active"=>1, "options" => ['Stress Level', 'High Stress', 'Low Stress']];

        $questions[5] = ["section"=>"feedback_record_day28", "question"=>"Can you carry out the exercises without using the videos?", "type"=>"radio", "is_active"=>1, "options" => ['Yes', 'No']];

        $questions[6] = ["section"=>"feedback_record_day28", "question"=>"Which of these exercises did you enjoy the most? (You can select more than one)", "type"=>"option", "is_active"=>1, "options" => ['Four Thumps', 'The Cross Over', 'Wayne Cook', 'Crown Pull', 'Heaven and Earth', 'Hook Up and Zip Up', 'None']];

        $questions[7] = ["section"=>"feedback_record_day28", "question"=>"How are your goals and aspirations met?", "type"=>"slider", "is_active"=>1, "options" => [
            'Mood', 'Not at all', 'Very Much', 'Physical Health', 'Not at all', 'Very Much', 'Mental Health', 'Not at all', 'Very Much', 'Stress Levels', 'Not at all'
        ]];

        $questions[8] = ["section"=>"feedback_record_day28", "question"=>"How likely is it that you'll continue to use the Daily Energy Routine every day?", "type"=>"slider", "is_active"=>1, "options" => ['Likeliness to continue use', 'Not Very', 'For Sure']];

        foreach($questions as $question)
        {
            $options = $question['options'];
            unset($question['options']);
            $question_id = Question::create($question)->id;
            foreach($options as $option)
            {
                Answer::create(['question_id'=>$question_id, 'label'=>$option]);
            }
        }

        $challenges = [];
        $challenges[0] = ['title'=>'The Four Thumps', 'description'=>'Y6-2673SlHY'];
        $challenges[1] = ['title'=>'Crossover Shoulder Pull', 'description'=>'3pmauevalC4'];
        $challenges[2] = ['title'=>'Cross Crawl', 'description'=>'05OCvXMQm0w'];
        $challenges[3] = ['title'=>'Wayne Cook Posture', 'description'=>'-o_H1zO6a4U'];
        $challenges[4] = ['title'=>'Crown Pull', 'description'=>'fwuHvtsIX1g'];
        $challenges[5] = ['title'=>'Connecting Heaven and Earth', 'description'=>'4gTw3GOu92E'];
        $challenges[6] = ['title'=>'Zip Up', 'description'=>'s21DaEh7-_g'];
        $challenges[7] = ['title'=>'Hook Up', 'description'=>'PTbaVadzsbA'];

        foreach($challenges as $challenge)
        {
            Challenges::create($challenge);
        }
    }
}
