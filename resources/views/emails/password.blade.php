<!DOCTYPE HTML>
<html>
<head>
  <meta charset="utf-8">
  <title>Welcome {{config('app.name')}}</title>
</head>
<body leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
	<center>
		<table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width:500px;padding: 0 20px;">
			<tr bgcolor="#ffffff">
                <td align="center" valign="top" style="padding-bottom: 20px;">
                    <table border="0" cellpadding="0" cellspacing="0">
                        <tbody>
                            <tr>
                                <td class="dont" style="text-align:center;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="center" style="padding: 20px 0px 40px 0;">
                                                    <a href="javascript:void(0)" target="_blank"><img src="{{ asset('asset/images/logo.png') }}" alt="" style="text-align: center;"></a>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td align="left" style="padding:20px 0;" >
                                                    <div style="font-family:Arial;text-align:left;color:#000;margin:0;padding:0;line-height:1;"><p>Uh oh! Don’t you hate it when you forget your password!</p><br><p>All good - We’ve got you!</p><br><p>Copy and paste the password below and you'll be good to go.</p><br><p>New Password: <b>{{ $password }}</b></p></div>
                                                </td>
                                            </tr>
                                            <tr>
                                                <td style="padding:20px 0; font-family:Arial, Helvetica, sans-serif;">
                                                  <p style="margin:0; line-height:20px; font-family:Arial, Helvetica, sans-serif; color:#000;">Thank You, <br>{{config('app.name')}} Support Team</p>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                            <tr>
                                <td class="dont" style="text-align:center;">
                                    <table border="0" cellpadding="0" cellspacing="0" width="100%">
                                        <tbody>
                                            <tr>
                                                <td align="left" style="padding:10px 0px;" >
                                                    <h2 style="font-family:Arial;text-align:left;color:#000;font-weight:400;margin:0;padding:0;line-height:1;font-size:16px;line-height: 1.5;">{{date('Y')}} &copy; {{config('app.name')}}. All rights reserved.</h2>
                                                </td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </td>
            </tr>
		</table>
	</center>
</body>
</html>
